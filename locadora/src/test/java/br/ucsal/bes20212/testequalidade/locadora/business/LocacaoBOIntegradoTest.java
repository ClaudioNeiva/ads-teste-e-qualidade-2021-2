package br.ucsal.bes20212.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20212.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * Testes INTEGRADOS.
 * 
 * @author claudioneiva
 *
 */
public class LocacaoBOIntegradoTest {

	private LocacaoBO locacaoBO;

	private VeiculoDAO veiculoDAO;

	@BeforeEach
	void setup() {
		veiculoDAO = new VeiculoDAO();
		locacaoBO = new LocacaoBO(veiculoDAO);
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 1 veículo fabricado em 2005.
	 */
	void testarCalculoValorTotalLocacao1Veiculo3Dias() {
	}

	/**
	 * Testar o cálculo do valor total de locação por 3 dias de 4 veículos fabricado há mais de 5 anos e 2 veículos
	 * fabricados há 1 ano.
	 * 
	 * Caso de teste:
	 * 
	 * Entrada de dados:
	 * 
	 * Placas = de 4 veículos fabricados há 6 anos e 2 veículos fabricados há 1 ano;
	 * 
	 * Quantidade de dias de locação = 3
	 * 
	 * Data de referência = data atual
	 * 
	 * Valores de diária = todas os veículos terão diária de 100 reais
	 * 
	 * Saída esperada:
	 * 
	 * Valor da locação = 1680
	 * 
	 * @throws VeiculoNaoEncontradoException
	 * 
	 */
	@Test
	void testarCalculoValorTotalLocacao6Veiculo3Dias() throws VeiculoNaoEncontradoException {
		// Dados de entrada
		LocalDate dataReferencia = LocalDate.now();
		int quantidadeDiasLocacao = 3;
		Double valorDiaria = 100d;

		int anoFabricacaoAntigo = dataReferencia.getYear() - 6;
		int anoFabricacaoNovo = dataReferencia.getYear() - 1;

		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(anoFabricacaoAntigo)
				.comDiaria(valorDiaria);

		Veiculo veiculo1 = veiculoBuilder.comPlaca("ABC-1234").build();
		Veiculo veiculo2 = veiculoBuilder.comPlaca("BCD-2345").build();
		Veiculo veiculo3 = veiculoBuilder.comPlaca("ERT-3452").build();
		Veiculo veiculo4 = veiculoBuilder.comPlaca("POT-3452").build();

		Veiculo veiculo5 = veiculoBuilder.fabricadoEm(anoFabricacaoNovo).comPlaca("ASD-8585").build();
		Veiculo veiculo6 = veiculoBuilder.comPlaca("RTG-7857").build();

		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		veiculoDAO.insert(veiculo5);
		veiculoDAO.insert(veiculo6);

		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca(), veiculo3.getPlaca(),
				veiculo4.getPlaca(), veiculo5.getPlaca(), veiculo6.getPlaca());

		// Saída esperada
		Double valorLocacaoEsperado = 1680d;

		// Executar o método de teste e obter a saída atual
		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataReferencia);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);

	}

}
