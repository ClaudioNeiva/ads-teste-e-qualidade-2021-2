package br.ucsal.bes20212.testequalidade.locadora.builder;

import java.time.LocalDate;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

// FIXME REMOVER essa classe, pois vamos utilizar o VeiculoBuilder no lugar do VeiculoMother.
public class VeiculoMother {

	public static Veiculo criarVeiculoAntigosDisponivel() {
		int anoRefAntigos = LocalDate.now().getYear() - 6;
		Modelo modelo1 = new Modelo("Gol");
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-1234");
		veiculo.setModelo(modelo1);
		veiculo.setAnoFabricacao(anoRefAntigos);
		veiculo.setValorDiaria(456d);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		return veiculo;
	}

	public static Veiculo criarVeiculoAntigosLocado() {
		int anoRefAntigos = LocalDate.now().getYear() - 6;
		Modelo modelo1 = new Modelo("Gol");
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-1234");
		veiculo.setModelo(modelo1);
		veiculo.setAnoFabricacao(anoRefAntigos);
		veiculo.setValorDiaria(456d);
		veiculo.setSituacao(SituacaoVeiculoEnum.LOCADO);
		return veiculo;
	}

	public static Veiculo criarVeiculoNovoDisponivel() {
		int anoRefAntigos = LocalDate.now().getYear() - 1;
		Modelo modelo1 = new Modelo("Gol");
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-1234");
		veiculo.setModelo(modelo1);
		veiculo.setAnoFabricacao(anoRefAntigos);
		veiculo.setValorDiaria(456d);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		return veiculo;
	}

	public static Veiculo criarVeiculoNovoEmManutencao() {
		int anoRefAntigos = LocalDate.now().getYear() - 1;
		Modelo modelo1 = new Modelo("Gol");
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("ABC-1234");
		veiculo.setModelo(modelo1);
		veiculo.setAnoFabricacao(anoRefAntigos);
		veiculo.setValorDiaria(456d);
		veiculo.setSituacao(SituacaoVeiculoEnum.MANUTENCAO);
		return veiculo;
	}
}
