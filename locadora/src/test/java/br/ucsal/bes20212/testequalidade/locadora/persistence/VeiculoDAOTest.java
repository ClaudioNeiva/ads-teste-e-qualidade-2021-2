package br.ucsal.bes20212.testequalidade.locadora.persistence;

import br.ucsal.bes20212.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;

public class VeiculoDAOTest {

	void testarInclusaoVeiculo() {
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoLocado();
		
		Veiculo veiculo1 = veiculoBuilder.build();
		Veiculo veiculo2 = veiculoBuilder.build();
		Veiculo veiculo3 = veiculoBuilder.build();
		Veiculo veiculo4 = veiculoBuilder.build();
		Veiculo veiculo5 = veiculoBuilder.build();
	}
	
}


//Veiculo veiculo = VeiculoMother.criarVeiculoAntigosDisponivel();
