package br.ucsal.bes20212.testequalidade.aula13;

import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxInvestingComTest extends AbstractInvestingComTest {

	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver-0.30.0");
		driver = new FirefoxDriver();
	}
	
}
