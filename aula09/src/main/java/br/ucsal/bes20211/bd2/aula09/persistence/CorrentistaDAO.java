package br.ucsal.bes20211.bd2.aula09.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;

public class CorrentistaDAO {

	private static final String QUERY_FIND_ALL = "select id, nm, nu_telefone, nu_ano_nascimento from correntista order by nm asc";

	private static final String QUERY_INSERT_FIXO = "insert into correntista (nm, nu_telefone, nu_ano_nascimento) values ('Neiva','122222',1995)";

	private static final String QUERY_INSERT = "insert into correntista (nm, nu_telefone, nu_ano_nascimento) values (?, ?, ?)";

	private static final String QUERY_UPDATE = "update correntista set nm=?, nu_telefone=?, nu_ano_nascimento=? where id=?";

	public List<Correntista> findAll() throws SQLException {
		List<Correntista> correntistas = new ArrayList<>();

		try (Statement stmt = DBUtil.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE); ResultSet rs = stmt.executeQuery(QUERY_FIND_ALL)) {
			while (rs.next()) {
				correntistas.add(resultSet2Correntista(rs));
			}
		}

		return correntistas;
	}

	public void persist(Correntista correntista) throws SQLException {
		try (PreparedStatement stmt = DBUtil.getConnection().prepareStatement(QUERY_INSERT,
				Statement.RETURN_GENERATED_KEYS)) {
			stmt.setString(1, correntista.getNome());
			stmt.setString(2, correntista.getTelefone());
			stmt.setInt(3, correntista.getAnoNascimento());
			stmt.executeUpdate();
			correntista.setId(getGeneratedId(stmt));
		}
	}

	public void merge(Correntista correntista) throws SQLException {
		try (PreparedStatement stmt = DBUtil.getConnection().prepareStatement(QUERY_UPDATE)) {
			stmt.setString(1, correntista.getNome());
			stmt.setString(2, correntista.getTelefone());
			stmt.setInt(3, correntista.getAnoNascimento());
			stmt.setInt(4, correntista.getId());
			stmt.executeUpdate();
		}
	}

	public int persist() throws SQLException {
		try (Statement stmt = DBUtil.getConnection().createStatement()) {
			stmt.executeUpdate(QUERY_INSERT_FIXO, Statement.RETURN_GENERATED_KEYS);
			return getGeneratedId(stmt);

		}
	}

	private Integer getGeneratedId(Statement stmt) throws SQLException {
		try (ResultSet idRs = stmt.getGeneratedKeys();) {
			idRs.next();
			return idRs.getInt(1);
		}
	}

	private Correntista resultSet2Correntista(ResultSet rs) throws SQLException {
		Integer id = rs.getInt("id");
		String nome = rs.getString("nm");
		String telefone = rs.getString("nu_telefone");
		Integer anoNascimento = rs.getInt("nu_ano_nascimento");
		return new Correntista(id, nome, telefone, anoNascimento);
	}

}
