package br.ucsal.bes20211.bd2.aula09.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	private static final String URL = "jdbc:postgresql://localhost:5432/aula-jdbc";

	private static Connection connection;

	private DBUtil() {
	}

	public static void connect(String user, String password) throws SQLException {
		connection = DriverManager.getConnection(URL, user, password);
	}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			throw new SQLException("Conexão não disponível.");
		}
		return connection;
	}

	public static void close() throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}

}
