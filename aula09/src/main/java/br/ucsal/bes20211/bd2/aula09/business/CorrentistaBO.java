package br.ucsal.bes20211.bd2.aula09.business;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

public class CorrentistaBO {

	private static final int TAMANHO_MINIMO_NOME = 10;

	private CorrentistaDAO correntistaDAO;

	public CorrentistaBO(CorrentistaDAO correntistaDAO) {
		this.correntistaDAO = correntistaDAO;
	}

	public void abrirConta(String nome, String telefone, Integer anoNascimento) throws NegocioException, SQLException {
		if (nome.trim().length() < TAMANHO_MINIMO_NOME) {
			throw new NegocioException("Nome inválido.");
		}
		List<Correntista> correntistasAtuais = correntistaDAO.findAll();
		for (Correntista correntista : correntistasAtuais) {
			if (correntista.getNome().equalsIgnoreCase(nome)) {
				throw new NegocioException("Já existe um correntista com este nome.");
			}
		}
		Correntista correntista = new Correntista(nome, telefone, anoNascimento);
		correntistaDAO.persist(correntista);
	}

}
