package br.ucsal.bes20211.bd2.aula09.infra;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

public class CorrentistaDAOFake extends CorrentistaDAO {

	private List<Correntista> correntistas = new ArrayList<>();

	@Override
	public void persist(Correntista correntista) throws SQLException {
		correntistas.add(correntista);
	}

	@Override
	public List<Correntista> findAll() throws SQLException {
		return new ArrayList<>(correntistas);
	}

}
