package br.ucsal.bes20211.bd2.aula09.infra;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.ucsal.bes20211.bd2.aula09.persistence.DBUtil;

public class CorrentistaDAOUtil {

	private static final String QUERY_UPDATE = "delete from correntista";

	public void deleteAll() throws SQLException {
		try (PreparedStatement stmt = DBUtil.getConnection().prepareStatement(QUERY_UPDATE)) {
			stmt.executeUpdate();
		}
	}

}
