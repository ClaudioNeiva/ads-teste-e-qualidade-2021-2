package br.ucsal.bes20211.bd2.aula09.business;

import java.sql.SQLException;
// Classe de testes unitários, com uso de de um dublê do tipo Fake
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.infra.CorrentistaDAOFake;

// Um teste unitário utilizando Fake
public class CorrentistaBO1Test {

	private CorrentistaBO correntistaBO;
	private CorrentistaDAOFake correntistaDAOFake;

	@BeforeEach
	void setup() throws SQLException {
		correntistaDAOFake = new CorrentistaDAOFake();
		correntistaBO = new CorrentistaBO(correntistaDAOFake);
	}

	// Executou em 0,028s
	@Test
	void testarIncluirCorrentistaValido() throws NegocioException, SQLException {
		String nome = "Antonio Claudio Neiva";
		String telefone = "123123123";
		Integer anoNascimento = 2010;

		Correntista correntistaEsperado = new Correntista(nome, telefone, anoNascimento);

		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		List<Correntista> correntistasAtuais = correntistaDAOFake.findAll();

		// Verificar que só existe 1 correntista na lista de correntistasAtuais
		Assertions.assertEquals(1, correntistasAtuais.size());

		// Verificar se o correntista existente é o correntista esperado
		Assertions.assertEquals(correntistaEsperado, correntistasAtuais.get(0));
	}

}
