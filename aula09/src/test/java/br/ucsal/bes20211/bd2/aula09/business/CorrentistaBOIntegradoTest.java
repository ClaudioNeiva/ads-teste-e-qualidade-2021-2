package br.ucsal.bes20211.bd2.aula09.business;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.infra.CorrentistaDAOUtil;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;
import br.ucsal.bes20211.bd2.aula09.persistence.DBUtil;

public class CorrentistaBOIntegradoTest {

	private CorrentistaBO correntistaBO;
	private CorrentistaDAO correntistaDAO;
	private CorrentistaDAOUtil correntistaDAOUtil;

	@BeforeEach
	void setup() throws SQLException {
		correntistaDAOUtil = new CorrentistaDAOUtil();
		correntistaDAO = new CorrentistaDAO();
		correntistaBO = new CorrentistaBO(correntistaDAO);
		DBUtil.connect("postgres", "abcd1234");
	}
	
	// Executou em 0,760s
	@Test
	void testarIncluirCorrentistaValido() throws NegocioException, SQLException {
		String nome = "Antonio Claudio Neiva";
		String telefone = "123123123";
		Integer anoNascimento = 2010;

		Correntista correntistaEsperado = new Correntista(nome, telefone, anoNascimento);

		// Garantindo um estado conhecido para a base de dados. Exemplo de framework para garantir o estado da base:
		// DBUnit.
		correntistaDAOUtil.deleteAll();

		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		List<Correntista> correntistasAtuais = correntistaDAO.findAll();

		// Verificar que só existe 1 correntista na lista de correntistasAtuais
		Assertions.assertEquals(1, correntistasAtuais.size());

		// Verificar se o correntista existente é o correntista esperado
		Assertions.assertEquals(correntistaEsperado, correntistasAtuais.get(0));
	}

}
