package br.ucsal.bes20211.bd2.aula09.tui;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;

public class CorrentistaTUITest {

	private CorrentistaTUI correntistaTUI;
	private Scanner scannerMock;
	private CorrentistaBO correntistaBOMock;
	private PrintStream outMock;

	@BeforeEach
	void setup() {
		scannerMock = Mockito.mock(Scanner.class);
		correntistaBOMock = Mockito.mock(CorrentistaBO.class);
		correntistaTUI = new CorrentistaTUI(scannerMock, correntistaBOMock);
		outMock = Mockito.mock(PrintStream.class);
		System.setOut(outMock);
	}

	@Test
	void testarCadastroCorrentistaValido() throws NegocioException, SQLException {
		String nome = "Antonio Claudio Neiva";
		String telefone = "123123123";
		Integer anoNascimento = 2010;

		// Configuração dos mocks
		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(telefone);
		Mockito.when(scannerMock.nextInt()).thenReturn(anoNascimento);

		correntistaTUI.cadastrar();

		Mockito.verify(correntistaBOMock).abrirConta(nome, telefone, anoNascimento);
		
		Assertions.assertAll("Verificação do outMock",
			() -> Mockito.verify(outMock).println("Informe o nome: "),
			() -> Mockito.verify(outMock).println("Informe o telefone: "),
			() -> Mockito.verify(outMock).println("Informe o ano de nascimento: "));
	}

}
