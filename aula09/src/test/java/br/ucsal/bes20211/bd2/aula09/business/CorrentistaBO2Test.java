package br.ucsal.bes20211.bd2.aula09.business;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;

// Teste unitário utilizando Mock.
public class CorrentistaBO2Test {

	private CorrentistaBO correntistaBO;
	private CorrentistaDAO correntistaDAOMock;

	@BeforeEach
	void setup() throws SQLException {
		correntistaDAOMock = Mockito.mock(CorrentistaDAO.class);
		correntistaBO = new CorrentistaBO(correntistaDAOMock);
	}

	// Executou em 0,363s
	@Test
	void testarIncluirCorrentistaValido() throws NegocioException, SQLException {
		String nome = "Antonio Claudio Neiva";
		String telefone = "123123123";
		Integer anoNascimento = 2010;

		Correntista correntistaEsperado = new Correntista(nome, telefone, anoNascimento);

		// Método abrirConta é um método do tipo command (não retorna nada).
		correntistaBO.abrirConta(nome, telefone, anoNascimento);

		// Quando estamos testando unitariamente um método command, utilizamos a
		// verificação de que certas chamadas de método ocorreram (com parâmetros específicos),
		// como uma verificação de sucesso do teste.
		Mockito.verify(correntistaDAOMock).persist(correntistaEsperado);

	}

}
