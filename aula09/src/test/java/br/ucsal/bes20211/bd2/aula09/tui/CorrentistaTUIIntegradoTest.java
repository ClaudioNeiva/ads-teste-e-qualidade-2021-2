package br.ucsal.bes20211.bd2.aula09.tui;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20211.bd2.aula09.business.CorrentistaBO;
import br.ucsal.bes20211.bd2.aula09.domain.Correntista;
import br.ucsal.bes20211.bd2.aula09.exception.NegocioException;
import br.ucsal.bes20211.bd2.aula09.infra.CorrentistaDAOUtil;
import br.ucsal.bes20211.bd2.aula09.persistence.CorrentistaDAO;
import br.ucsal.bes20211.bd2.aula09.persistence.DBUtil;

public class CorrentistaTUIIntegradoTest {

	private CorrentistaTUI correntistaTUI;
	private Scanner scannerMock;
	private CorrentistaBO correntistaBO;
	private CorrentistaDAO correntistaDAO;
	private CorrentistaDAOUtil correntistaDAOUtil;

	@BeforeEach
	void setup() throws SQLException {
		scannerMock = Mockito.mock(Scanner.class);
		correntistaDAOUtil = new CorrentistaDAOUtil();
		correntistaDAO = new CorrentistaDAO();
		correntistaBO = new CorrentistaBO(correntistaDAO);
		correntistaTUI = new CorrentistaTUI(scannerMock, correntistaBO);
		DBUtil.connect("postgres", "abcd1234");
	}

	@Test
	void testarCadastroCorrentistaValido() throws NegocioException, SQLException {
		String nome = "Antonio Claudio";
		String telefone = "123123123";
		Integer anoNascimento = 2010;

		Correntista correntistaEsperado = new Correntista(nome, telefone, anoNascimento);

		// Configuração dos mocks
		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(telefone);
		Mockito.when(scannerMock.nextInt()).thenReturn(anoNascimento);

		correntistaDAOUtil.deleteAll();

		correntistaTUI.cadastrar();
		
		List<Correntista> correntistasAtuais = correntistaDAO.findAll();

		// Verificar que só existe 1 correntista na lista de correntistasAtuais
		Assertions.assertEquals(1, correntistasAtuais.size());

		// Verificar se o correntista existente é o correntista esperado
		Assertions.assertEquals(correntistaEsperado, correntistasAtuais.get(0));
	}

}
