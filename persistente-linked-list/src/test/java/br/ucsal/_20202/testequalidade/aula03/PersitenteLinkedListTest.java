package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

class PersitenteLinkedListTest {

	private PersistentList<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new PersitenteLinkedList<>();
	}

	@Test
	void testarPersistirLista3Nomes()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		// Restrição - verificar se o banco de dados está no ar.
		Assumptions.assumeTrue(DbUtil.isConnectionValid());

		// Dados de entrada
		String nome1 = "antonio";
		String nome2 = "claudio";
		String nome3 = "neiva";
		nomes.add(nome1);
		nomes.add(nome2);
		nomes.add(nome3);
		// Saída esperada
		// São os mesmos nomes dos dados de entrada, na mesma ordem, em uma outra lista.

		// Executar o método que está sendo testado
		Long idLista = 1L;
		Connection connection = DbUtil.getConnection();
		String nomeTab = "lista1";
		nomes.persist(idLista, connection, nomeTab);

		// Obter o resultado atual
		PersistentList<String> nomesAtual = new PersitenteLinkedList<>();
		nomesAtual.load(idLista, connection, nomeTab);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertAll("Verificação de nomes", () -> Assertions.assertEquals(nome1, nomesAtual.get(0)),
				() -> Assertions.assertEquals(nome2, nomesAtual.get(1)),
				() -> Assertions.assertEquals(nome3, nomesAtual.get(2)),
				() -> Assertions.assertNotSame(nomes, nomesAtual));

	}
}
