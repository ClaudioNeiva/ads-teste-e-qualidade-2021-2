package br.ucsal._20202.testequalidade.aula03.util;

// FIXME Utilizar o JUnit nesta classe!!! 
public class CalculoUtilSemJUnitTest {

	public static void main(String[] args) {
		testarFatorial0();
		testarFatorial5();
	}

	private static void testarFatorial0() {
		// Definir dados de entrada
		int n = 0;
		// Definir a saída esperada
		long fatorialEsperado = 1;

		// Executar o método que está sendo testado
		// Obter a saída a atual (saída que o método que sendo testado apresentou)
		CalculoUtil calculoUtil = new CalculoUtil();
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		if (fatorialEsperado == fatorialAtual) {
			System.out.println("testarFatorial0 - Sucesso");
		} else {
			System.out.println("testarFatorial0 - Falha  - esperava " + fatorialEsperado + " mas o resultado atual é " + fatorialAtual);
		}
	}

	private static void testarFatorial5() {
		// Definir dados de entrada
		int n = 5;
		// Definir a saída esperada
		long fatorialEsperado = 120;

		// Executar o método que está sendo testado
		// Obter a saída a atual (saída que o método que sendo testado apresentou)
		CalculoUtil calculoUtil = new CalculoUtil();
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		if (fatorialEsperado == fatorialAtual) {
			System.out.println("testarFatorial5 - Sucesso");
		} else {
			System.out.println("testarFatorial5 - Falha  - esperava " + fatorialEsperado + " mas o resultado atual é " + fatorialAtual);
		}
	}

}
