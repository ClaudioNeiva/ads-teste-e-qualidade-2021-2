package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}

	@Test
	void testarAdd3Nomes() throws InvalidElementException {
		// Dados de entrada
		String nome1 = "antonio";
		String nome2 = "claudio";
		String nome3 = "neiva";
		// Saída esperada
		// São os mesmos nomes dos dados de entrada, na mesma ordem

		// Executar o método que está sendo testado
		nomes.add(nome1);
		nomes.add(nome2);
		nomes.add(nome3);

		// Obter o resultado atual
		String nomeAtual1 = nomes.get(0);
		String nomeAtual2 = nomes.get(1);
		String nomeAtual3 = nomes.get(2);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertAll("Verificação de nomes", () -> Assertions.assertEquals(nome1, nomeAtual1),
				() -> Assertions.assertEquals(nome2, nomeAtual2), () -> Assertions.assertEquals(nome3, nomeAtual3));
	}

	@Test
	void testarAddElementoNulo() {
		// Dado de entrada
		String nome = null;
		// Saída esperada é a ocorrência da exceção InvalidElementException
		String mensagemEsperada = "Elemento nulo não pode ser adicionado à lista.";

		// Executar o método que está sendo testado
		// Obter o resultado atual
		// Comparar o resultado esperado com o resultado atual (verificar se a exceção
		// foi lançada)
		InvalidElementException exception = Assertions.assertThrows(InvalidElementException.class,
				() -> nomes.add(nome));
		Assertions.assertEquals(mensagemEsperada, exception.getMessage());
	}

	@Test
	void testarGetIndiceNaoValido() throws InvalidElementException {
		String nome1 = "antonio";
		String nome2 = "claudio";
		nomes.add(nome1);
		nomes.add(nome2);
		String nomeAtual = nomes.get(2);
		Assertions.assertNull(nomeAtual);
	}

}
