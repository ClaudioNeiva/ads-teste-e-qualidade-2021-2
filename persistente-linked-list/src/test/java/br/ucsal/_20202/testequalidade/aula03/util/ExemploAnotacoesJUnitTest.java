package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ExemploAnotacoesJUnitTest {

	@BeforeAll
	static void setupClass() {
		System.out.println("setupClass...estabelecer uma conexão com o SGBD que será utilizada pelos testes...");
	}

	@BeforeEach
	void setup() {
		System.out.println("	setup...iniciar uma transação (begin transaction)...");
	}

	@AfterEach
	void teardown() {
		System.out.println("	teardown...fazer um rollback...");
	}

	@AfterAll
	static void teardownClass() {
		System.out.println("teardownClass...desconectar o SGBD...");
	}

	@Test
	void test1() {
		System.out.println("		test1, que utiliza a conexão que foi aberta no setupClass, no contexto da transação iniciada no setup");
	}

	@Test
	@DisplayName("Verificar se os dados do aluno foram enviados para o MEC.")
	void test2() {
		System.out.println("		test2, que utiliza a conexão que foi aberta no setupClass, no contexto da transação iniciada no setup");
	}

	@Test
	@Disabled
	void test3() {
		System.out.println("		test3, que utiliza a conexão que foi aberta no setupClass, no contexto da transação iniciada no setup");
	}

	@Test
	void test4() {
		System.out.println("		test4, que utiliza a conexão que foi aberta no setupClass, no contexto da transação iniciada no setup");
	}

}
